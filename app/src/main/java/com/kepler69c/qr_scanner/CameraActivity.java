package com.kepler69c.qr_scanner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Size;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/*
 **CameraActivity Structure**

public class
    vars
    onCreate
        onClickListeners
        + cameraProvideFuture
    BindImageAnalysis
        analyze
    decodeQrImage (for analyze)
    toBitmap (for analyze)
    onActivityResult (copied from old qr_reader app)
    closedBottomsheet (onClose BottomsheetActivity)
*/

public class CameraActivity extends AppCompatActivity {
    boolean qr_found = false;
    Bitmap bitmap;
    String code;
    boolean FlashOn = false;

    private PreviewView previewView;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        previewView = findViewById(R.id.previewView);
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        Button select = findViewById(R.id.select);
        select.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            launchSomeActivity.launch(intent);
        });

        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindImageAnalysis(cameraProvider);
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }, ContextCompat.getMainExecutor(this));
    }


    private void bindImageAnalysis(@NonNull ProcessCameraProvider cameraProvider) {
        ImageAnalysis imageAnalysis =
                new ImageAnalysis.Builder().setTargetResolution(new Size(1280, 720))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST).build();
        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(this), image -> {
            if (!qr_found) {
                @SuppressLint({"UnsafeExperimentalUsageError", "UnsafeOptInUsageError"}) Image img = image.getImage();
                assert img != null;
                bitmap = toBitmap(img);

                CvFunctions cvfunctions = new CvFunctions();
                Map.Entry<Boolean, Bitmap> returnEntry = cvfunctions.findCode(bitmap);
                Boolean qrFound = returnEntry.getKey();
                Bitmap bitmap = returnEntry.getValue();

                if (qrFound) {
                    try {
                        code = decodeQRImage(bitmap);
                        qr_found = true;
                        Toast.makeText(getApplicationContext(), code, Toast.LENGTH_SHORT).show();

                        BottomsheetActivity bottomSheet = new BottomsheetActivity();
                        Bundle bundle = new Bundle();
                        bundle.putString("code", code);
                        bottomSheet.setArguments(bundle);
                        bottomSheet.show(getSupportFragmentManager(), "ModalBottomSheet");
                    } catch (ChecksumException | NotFoundException | FormatException e) {
                        e.printStackTrace();
                    }
                }
            }
            image.close();
        });
        Preview preview = new Preview.Builder().build();
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        preview.setSurfaceProvider(previewView.getSurfaceProvider());

        Camera cam = cameraProvider.bindToLifecycle(this, cameraSelector,
                imageAnalysis, preview);

        Button button = findViewById(R.id.flash);
        button.setOnClickListener(view -> {
            FlashOn = !FlashOn;
            cam.getCameraControl().enableTorch(FlashOn);
        });
    }

    public static String decodeQRImage(Bitmap qrBitmap) throws ChecksumException, NotFoundException, FormatException {

        int[] intArray = new int[qrBitmap.getWidth()* qrBitmap.getHeight()];
        //copy pixel data from the Bitmap into the 'intArray' array
        qrBitmap.getPixels(intArray, 0, qrBitmap.getWidth(), 0, 0, qrBitmap.getWidth(), qrBitmap.getHeight());

        LuminanceSource source = new RGBLuminanceSource(qrBitmap.getWidth(), qrBitmap.getHeight(), intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Reader reader = new MultiFormatReader();
        Result result = reader.decode(bitmap);

        return result.getText();
    }

    private Bitmap toBitmap(Image image) {
        Image.Plane[] planes = image.getPlanes();
        ByteBuffer yBuffer = planes[0].getBuffer();
        ByteBuffer uBuffer = planes[1].getBuffer();
        ByteBuffer vBuffer = planes[2].getBuffer();

        int ySize = yBuffer.remaining();
        int uSize = uBuffer.remaining();
        int vSize = vBuffer.remaining();

        byte[] nv21 = new byte[ySize + uSize + vSize];
        //U and V are swapped
        yBuffer.get(nv21, 0, ySize);
        vBuffer.get(nv21, ySize, vSize);
        uBuffer.get(nv21, ySize + vSize, uSize);

        YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, image.getWidth(), image.getHeight(), null);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, yuvImage.getWidth(), yuvImage.getHeight()), 75, out);

        byte[] imageBytes = out.toByteArray();
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    //    activity result launcher if an image is selected
    ActivityResultLauncher<Intent> launchSomeActivity
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data != null && data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                            findCode(bitmap, true);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

    public void findCode(Bitmap bitmap, Boolean toastStatus) {
        try {
            CvFunctions cvfunctions = new CvFunctions();
            Map.Entry<Boolean, Bitmap> returnEntry = cvfunctions.findCode(bitmap);
            Boolean qrFound = returnEntry.getKey();
            Bitmap qrBmp = returnEntry.getValue();

            if (qrFound) {
                code = decodeQRImage(qrBmp);
                qr_found = true;
                Toast.makeText(getApplicationContext(), code, Toast.LENGTH_SHORT).show();

                BottomsheetActivity bottomSheet = new BottomsheetActivity();
                Bundle bundle = new Bundle();
                bundle.putString("code", code);
                bottomSheet.setArguments(bundle);
                bottomSheet.show(getSupportFragmentManager(), "ModalBottomSheet");
            } else if (toastStatus) {
                Toast.makeText(getApplicationContext(), "no QR-Code found by CvFunctions", Toast.LENGTH_SHORT).show();
            }

        }
        catch (ChecksumException | FormatException e) {
            e.printStackTrace();
        } catch (NotFoundException e) {
            if (toastStatus) {
                Toast.makeText(getApplicationContext(), "no QR-Code found", Toast.LENGTH_SHORT).show();
            }
            e.printStackTrace();
        }
    }

    public void closedBottomsheet(){qr_found = false;}

}
